<?php


    namespace Prophet\ProphetResources\Facades;

    use Illuminate\Support\Facades\Facade;


    class ProphetResources extends Facade
    {
        protected static function getFacadeAccessor() { return 'ProphetResources'; }
    }